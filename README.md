# Boss::AdvancedSearch

This gem includes some JavaScript that will assist you in creating a Ransack-based advanced search form.

## Installation

Add these lines to your application's Gemfile (boss-advanced_search depends on jQuery):

```ruby
gem 'jquery-rails'
gem 'boss-advanced_search', git: 'https://bitbucket.org/bossgit/boss-advanced_search.git', tag: 'v0.1.0'
```

And then execute:

    $ bundle

## Usage

A bit too complicated to explain here... Check out CI search on BOSSDesk or Ticket search on BOSS811 for code samples on how to implement.

## Versioning

This gem uses [semantic versioning](http://semver.org) which means, given a version number MAJOR.MINOR.PATCH, increment the:

1. MAJOR version when you make incompatible API changes,
2. MINOR version when you add functionality in a backwards-compatible manner, and
3. PATCH version when you make backwards-compatible bug fixes.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
