this.Search = (function() {
  function Search() {}

  Search.prototype.remove_fields = function(button) {
    var filter = $(button).closest('.fields');
    var group = filter.parent().closest('.fields');
    if (group.find('.fields').length > 1)
      filter.remove();
    else
      group.remove();
  };

  Search.prototype.add_fields = function(button, type, content) {
    var new_id = new Date().getTime();
    var regexp = new RegExp('new_' + type, 'g');
    $(button).before(content.replace(regexp, new_id));

    if (type === 'condition') {
      var filter = $(button).parent().find('.condition').last();
      populate_predicate_select(filter);
      this.on_predicate_select(filter.find('.predicate'));
      this.set_up_value_input(filter);
      this.make_custom_field_specific_changes(filter);
    }
  };

  Search.prototype.on_attribute_change = function(select) {
    var filter = select.closest('.fields');
    filter.find('.value').val('');
    populate_predicate_select(filter);
    set_up_value_input(filter);
    on_predicate_select(filter.find('.predicate'));
    make_custom_field_specific_changes(filter);
  };

  Search.prototype.set_up_value_input = function(filter) {
    set_up_value_input(filter);
  };

  Search.prototype.on_predicate_select = function(select) {
    on_predicate_select(select);
  }

  Search.prototype.make_custom_field_specific_changes = function(filter) {
    make_custom_field_specific_changes(filter);
  }

  return Search;
})();

function populate_predicate_select(condition_block) {
  var attribute_name = condition_block.find('.attribute').val();
  var predicate_select = condition_block.find('.predicate');
  predicate_select.html('');
  $.each(ModelAttributePredicates[attribute_name], function(key, value) {
    predicate_select.append($('<option />').val(key).text(value));
  });
}

function on_predicate_select(select) {
  var predicate = select.val();
  var value_container = select.closest('.fields').find('.value-container');
  var value_input = value_container.find('.value');

  if (['present', 'blank', 'null', 'not_null', 'true', 'false',
    'today', 'yesterday', 'this_week', 'last_week', 'this_month', 'last_month', 'in_the_past',
    'not_zero'].indexOf(predicate) !== -1) {
    value_container.hide();
    value_input.val('1');
  } else {
    value_container.show();
  }
}

function is_custom_field_attribute(attr_name) {
  return $.isNumeric(attr_name);
}

function make_custom_field_specific_changes(filter) {
  var attribute = filter.find('.attribute');
  var attribute_name = attribute.val();
  var attribute_select_name = attribute.attr('name');
  var ransacker_args_input_name = filter.attr('data-object-name') + '[a][0][ransacker_args]';

  // remove possibly existing custom field hidden fields
  filter.find('input:hidden[name="' + attribute_select_name + '"]').remove();
  filter.find('input:hidden[name="' + ransacker_args_input_name + '"]').remove();

  if (is_custom_field_attribute(attribute_name)) {
    var attribute_select_name_overwrite = $('<input/>', {
      type: 'hidden',
      name: attribute_select_name,
      value: 'custom_fields'
    });
    var ransacker_args = $('<input/>', {
      type: 'hidden',
      name: ransacker_args_input_name,
      value: attribute_name + ',' + ModelAttributeValueInputs[attribute_name].type_id
    });
    attribute.after(ransacker_args);
    attribute.after(attribute_select_name_overwrite);
  }
}

function create_input_group(input, icon) {
  var input_group = $('<div/>').addClass('input-group').addClass('date').append($('<span/>').addClass('input-group-addon').append($('<i/>').addClass('fa ' + icon)));
  var input = $('<input/>', {
    type: 'text',
    class: input.attr('class'),
    name: input.attr('name'),
    value: input.val()
  });
  return input_group.append(input);
}

function set_up_value_input(filter) {
  var attribute = filter.find('.attribute').val();
  var value_container = filter.find('.value-container');
  var value_input = filter.find('.value');
  var value_input_name = value_input.attr('name');
  var value_input_classes = value_input.attr('class');
  var data_type = ModelAttributeValueInputs[attribute]['type'];

  value_container.html('');
  if (data_type === 'string') {
    var input = $('<input/>', {
      type: 'text',
      class: value_input_classes,
      name: value_input_name,
      value: value_input.val()
    });
    value_container.append(input);
  } else if (data_type === 'date') {
    var input_group = create_input_group(value_input, 'fa-calendar');
    build_date_picker(input_group.find('.value'));
    value_container.append(input_group);
  } else if (data_type === 'select') {
    var input = $('<select/>', {
      class: value_input_classes,
      name: value_input_name
    });
    input.html(ValueInputSelects[ModelAttributeValueInputs[attribute]['data_source']]);
    input.val(value_input.val());
    value_container.append(input);
    if (ModelAttributeValueInputs[attribute]['data_source'] === 'response_codes') {
      value_container.addClass('with-response-code-select');
      input.chosen();
    } else {
      value_container.removeClass('with-response-code-select');
    }
  } else if (data_type === 'autocomplete') {
    var hidden = $('<input/>', {
      type: 'hidden',
      class: 'value',
      name: value_input_name,
      value: value_input.val()
    });
    var text_input = $('<input/>', {
      type: 'text',
      class: value_input_classes
    });
    var input_group = create_input_group(text_input, 'fa-keyboard-o');
    var input = input_group.find('.value');
    input.attr('placeholder', 'Type a value to search');
    value_container.append(hidden);
    value_container.append(input_group);

    var data_source = ModelAttributeValueInputs[attribute]['data_source'];
    autocomplete(input, hidden, '/' + data_source, 2);
    // check if the value is set, if it is, we'll load the labels
    if ($.isNumeric(value_input.val())) {
      $.get('/' + data_source + '/' + value_input.val(),
        function(data) {
          input.val(data[ModelAttributeValueInputs[attribute]['data_label']]);
        },
        'json'
      );
    }
  }
}
