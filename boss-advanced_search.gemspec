# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'boss/advanced_search/version'

Gem::Specification.new do |spec|
  spec.name          = 'boss-advanced_search'
  spec.version       = Boss::AdvancedSearch::VERSION
  spec.authors       = ['Jaanus Jaakma']
  spec.email         = ['jaanus@glenworx.com']

  spec.summary       = 'JavaScript logic for your advanced search!'
  spec.description   = 'This gem includes some JavaScript that you can use to build a Ransack advanced search form for your models.'
  spec.homepage      = 'https://bitbucket.org/bossgit/boss-advanced_search'
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = 'https://gems.boss-solutions.com'
  else
    raise 'RubyGems 2.0 or newer is required to protect against public gem pushes.'
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'railties', '>= 4.0', '< 5.1'
  spec.add_dependency 'jquery-rails', '~> 4.0'
  spec.add_development_dependency 'bundler', '~> 1.11'
  spec.add_development_dependency 'rake', '~> 10.0'
end
